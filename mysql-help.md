-----Sources-----
https://www.digitalocean.com/community/tutorials/how-to-create-and-manage-databases-in-mysql-and-mariadb-on-a-cloud-server
https://stackoverflow.com/questions/2995054/access-denied-for-user-rootlocalhost-using-passwordno

https://www.w3schools.com/sql/default.asp



Note: These commands are not case sensitive. They can be upper or lowercase.

Any commands that follow mysql> should be run in the mysql shell.

Start mysql shell as root
------------------------

mysql -u root -p



User commands
------------------------


mysql> CREATE USER 'newuser'@'localhost' IDENTIFIED BY 'password';
mysql> GRANT ALL PRIVILEGES ON * . * TO 'newuser'@'localhost';
mysql> FLUSH PRIVILEGES -p 'password'; (Replace with actual password)

Here is a short list of other common possible permissions that users can enjoy.

    ALL PRIVILEGES- as we saw previously, this would allow a MySQL user full access to a designated database (or if no database is selected, global access across the system)
    CREATE- allows them to create new tables or databases
    DROP- allows them to them to delete tables or databases
    DELETE- allows them to delete rows from tables
    INSERT- allows them to insert rows into tables
    SELECT- allows them to use the SELECT command to read through databases
    UPDATE- allow them to update table rows
    GRANT OPTION- allows them to grant or remove other users’ privileges

To provide a specific user with a permission, you can use this framework:

mysql> GRANT type_of_permission ON database_name.table_name TO 'username'@'localhost';

mysql> REVOKE type_of_permission ON database_name.table_name FROM 'username'@'localhost'; 

mysql> SHOW GRANTS FOR 'username'@'localhost';

mysql> DROP USER 'username'@'localhost';


Note: Some posts will show that the last underscore as a period. This is true for mysql 8.0+, but not for the lower versions.

I use 7.6, so I have underscores.

mysql> SET GLOBAL validate_password_length = 4;
mysql> SET GLOBAL validate_password_policy=LOW;



-------Database commands------

CREATE DATABASE new_database;

CREATE DATABASE IF NOT EXISTS new_database;

SHOW DATABASES;

We have received a result of "null". This means that no database is currently selected.

To select a database to use for subsequent operations, use the following commands:

You will need to use, and then select it.

USE new_database;

SELECT database();

Delete a database the same way you would a user.

DROP DATABASE new_database;

DROP DATABASE IF EXISTS new_database;


-----Manipulating tables-----


This is how to create a table with its attributes.

mysql> CREATE TABLE pet (name VARCHAR(20), owner VARCHAR(20),
       species VARCHAR(20), sex CHAR(1), birth DATE, death DATE);

To add attributes to an already existing table...

mysql> ALTER TABLE table_name
mysql> ADD column_name datatype;

Exhaustive list of all sql datatypes: https://www.w3schools.com/sql/sql_datatypes.asp

mysql> SHOW TABLES;

Output what our table looks like.

mysql> DESCRIBE table_name;


mysql> INSERT INTO TABLENAME(COLUMN_1, COLUMN_2,..)
    -> VALUES (VALUE_1,VALUE_2,..)

In my case, I did this: 
mysql> insert into client(name, pay) values ("Josh", 420.69);

After adding an element to the table, you can verify the input with the command:

mysql> select * from table_name; 

You can insert the current date by using now(). For example:
INSERT INTO table_name(f1, f2, fdate) VALUES ("Sum", "Ting", now());

For money, it's best to use a DECIMAL (not a float) since we only need to round to two decimal places. 
DECIMAL(5,2) means to create a decimal of maximum 5 digits, and 2 decimal places (ideal for money)
mysql> create table transaction (name VARCHAR(20), payment DECIMAL(5,2), paydate DATE);

You can sort the tables by doing:

SELECT 
   select_list
FROM 
   table_name
ORDER BY 
   column1 [ASC|DESC], (Do not include brackets)
   column2 [ASC|DESC],
   ...;

My command ended up  being:

SELECT payment FROM transaction ORDER BY payment DESC;

or for full information

SELECT * FROM transaction ORDER BY payment DESC;

Select only the transactions with 'Matt'

select * from transaction WHERE name = 'Matt';




To sum all elements in the "payment" column in the "transaction" table...

mysql> SELECT SUM(payment)
    -> FROM transaction
    -> WHERE condition;

We can also count and find the average with...

SELECT COUNT(column_name)
FROM table_name
WHERE condition; 

SELECT AVG(column_name)
FROM table_name
WHERE condition; 

Note: the WHERE condition is completely optional, it can be omitted if you don't want to
check for any conditions.

-----------------
In syntax,

First, you must specify the name of the table. After that, in parenthesis, 
you must specify the column name of the table, and columns must be separated by a comma
The values that you want to insert must be inside the parenthesis, and it must be followed by the 
VALUES clause 

If you want to insert more than one row at the same time, the syntax is slightly different. 
In the value keyword, you must specify a comma-separated list of the rows. 
Here, each element is considered as one row. All the rows must be comma-separated. 
The following is the syntax:

mysql> INSERT INTO <TABLENAME>(COLUMN_1, COLUMN_2,..)
	   VALUES 
	   (VALUE_1,VALUE_2,..), 
	   (VALUE_3,VALUE_4,..), 
	   (VALUE_5,VALUE_6,..)

-----Deleting elements-----

# Delete all elements containing the customer name 'Alfreds Futterkiste'
DELETE FROM Customers WHERE CustomerName='Alfreds Futterkiste';

# Delete a specific element based on its attributes
DELETE FROM transaction WHERE name = 'Matt' AND payment = 70;



------Sample database-------

https://www.mysqltutorial.org/how-to-load-sample-database-into-mysql-database-server.aspx
https://www.mysqltutorial.org/mysql-sample-database.aspx 

After downloading and unzipping, source the file with the command below.

mysql> source c:\temp\mysqlsampledatabase.sql

Now, we can use and list the database with the following commands:

USE classicmodels;

SELECT * FROM customers;

------------MySQL Sample Database Schema------------

The MySQL sample database schema consists of the following tables:
    Customers: stores customer’s data.
    Products: stores a list of scale model cars.
    ProductLines: stores a list of product line categories.
    Orders: stores sales orders placed by customers.
    OrderDetails: stores sales order line items for each sales order.
    Payments: stores payments made by customers based on their accounts.
    Employees: stores all employee information as well as the organization structure such as who reports to whom.
	Offices: stores sales office data.

------PDF of the sample database diagram-----------
https://www.mysqltutorial.org/wp-content/uploads/2018/04/MySQL-Sample-Database-Diagram-PDF-A4.pdf


Therefore, we can run commands like:

select * from offices;
select * from orders;

etc... sql makes all of this quite easy.
