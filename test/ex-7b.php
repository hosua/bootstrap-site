<?php declare(strict_types=1);
	
	// Flip coin n times
	function flipCoin($n){
		$flips = "";
		for ($x = 0; $x < $n; $x++){
			$coin = rand(0, 1);		
			if ($coin == 1) {
				// echo "H";
				$flips .= "H";
			}
			if ($coin == 0){
				// echo "T";
				$flips .= "T";
			}
		}	
		$flipsArr = str_split($flips);
		$heads = 0;
		$tails = 0;
		foreach ($flipsArr as $flip){
			if ($flip == 'H'){
				$heads  += 1;	
			} else {
				$tails += 1;
			}
		}
		echo "$flips <br>";
		echo "Heads: $heads <br> Tails: $tails <br>";
	}

	$rng = rand(1, 10);
	echo "$rng <br>";
	$rng = rand(-2, 20);
	echo "$rng <br>";
	$rng = rand(0, 11);
	echo "$rng <br>";

	flipCoin(100);	
?>

