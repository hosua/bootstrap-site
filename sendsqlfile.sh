file="$1"
dbname="$2"

if [ -z "$dbname" ]; then
	echo Incorrect usage: arg1 is the filename, arg2 is the database name.
fi

mysql -u root -p "$dbname" < "$file"
