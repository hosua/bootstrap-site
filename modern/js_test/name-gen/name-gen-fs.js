// Requiring fs module in which 
// readFile function is defined.


function generateNames(n=1){
	const fs = require('fs');
	var namesList = [];
	var numFirst = 0;
	var numLast = 0;
	const fnames = fs.readFileSync('first-names.txt').toString(); 
	const lnames = fs.readFileSync('last-names.txt').toString(); 
	numFirst = fnames.split('\n').length;
	numLast = lnames.split('\n').length;
	console.log(numFirst);
	console.log(numLast);
	for (let i = 0; i < n; i++){
		var rndFirst = Math.floor(Math.random() * numFirst);
		var rndLast = Math.floor(Math.random() * numLast);

		console.log(rndFirst);
		console.log(rndLast);

		var first_name = fnames.split("\n")[rndFirst];
		var last_name = lnames.split("\n")[rndLast];
		console.log(first_name + " " + last_name);
		namesList.push(first_name + " " + last_name);
	}
	return namesList;
}

function setName(textArea) {
	var names = generateNames(1);
	var names_string = "";
	for (let i = 0; i < names.length; i++){
		names_string += names[i] + "\n";	
	}
	document.getElementById(textArea).value+="names_string\n";
}
function clearName(textArea) {
	document.getElementById(textArea).value="";
}


