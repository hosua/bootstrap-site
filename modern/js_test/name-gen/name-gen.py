import random

n = int(input('How many names do you want to generate?\n'))

first_name_file = 'first-names.txt'
last_name_file = 'last-names.txt'

num_first = sum(1 for line in open(first_name_file))
num_last = sum(1 for line in open(last_name_file))

name_list = []

for i in range(n):
    rand_first = random.randrange(num_first)
    rand_last = random.randrange(num_last)
    first_name = ""
    last_name = ""
    with open(first_name_file) as fi:
        for j, line in enumerate(fi):
            if j == rand_first:
                first_name = line.strip()
                break
    fi.close()

    with open(last_name_file) as fi:
        for j, line in enumerate(fi):
            if j == rand_last:
                last_name = line.strip()
                break
    print(first_name + " " + last_name)
    fi.close()
print(name_list)
name_list.append((first_name, last_name))
