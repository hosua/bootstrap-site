user="$1"

if [ -z "$user" ]; then
	echo "Please enter username as an argument."
	exit
fi

mysql -u "$user" -p
