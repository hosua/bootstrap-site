If after a reboot, all the phpBB pages begin displaying white pages again,
change the oinstall folder back to install. Open the 
www.website.com/install/app.php in your browser, and update phpBB.
Everything should be working again after that.
